import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginGuard } from './shared/services/login.guard';
import { ReactiveFormsModule } from '@angular/forms';
import { LayoutComponent } from './shared/layout/layout.component';
import { TopnavComponent } from './shared/layout/topnav/topnav.component';
import { AngularMaterialModule } from './angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SidebarComponent } from './shared/layout/sidebar/sidebar.component';
import { LoginComponent } from './app/login/login.component';
import { ScanJdComponent } from './app/scan-jd/scan-jd.component';
import { ScanCandidateProfileComponent } from './app/scan-candidate-profile/scan-candidate-profile.component';
import { JdListingComponent } from './app/jd-listing/jd-listing.component';


const routes: Routes = [
  {path:'',redirectTo:'login', pathMatch: 'full' },
  {
    path: 'login',
    component: LoginComponent,
    pathMatch: 'full',
  },
  {
    path: 'home',
    component: LayoutComponent,   
    // canActivate: [LoginGuard],
    children:[      
      {
        path: '',
        component: ScanJdComponent,       
        pathMatch: 'full'       
      },
      {
        path: 'scanJD',
        component: ScanJdComponent,       
        pathMatch: 'full'       
      },
      {
        path: 'scanCandidateProfile',
        component: ScanCandidateProfileComponent,       
        pathMatch: 'full'       
      },
      {
        path: 'jdlisting',
        component: JdListingComponent,       
        pathMatch: 'full'       
      }
    ]
  }  
];

@NgModule({
  declarations:[LayoutComponent,TopnavComponent,SidebarComponent],
  imports: [RouterModule.forRoot(routes), ReactiveFormsModule, AngularMaterialModule, FlexLayoutModule],
  exports: [RouterModule, AngularMaterialModule, FlexLayoutModule]
})
export class AppRoutingModule { }
