import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jd-listing',
  templateUrl: './jd-listing.component.html',
  styleUrls: ['./jd-listing.component.css']
})
export class JdListingComponent implements OnInit {

 ELEMENT_DATA: any[] = [
  {jobProfile: 'Angular Developer', technicalSkill: 'Angular', softSkill: 'abc', experience: '5', education:'btech',jobType: 'Angular',jobLocation:'Noida.', action:''},
  ];

  constructor() { }
 
  ngOnInit(): void {
  }
  displayedColumns: string[] = ['jobProfile', 'technicalSkill', 'softSkill', 'experience','education','jobType','jobLocation','action'];
  dataSource = this.ELEMENT_DATA;
}
