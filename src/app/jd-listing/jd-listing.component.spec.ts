import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JdListingComponent } from './jd-listing.component';

describe('JdListingComponent', () => {
  let component: JdListingComponent;
  let fixture: ComponentFixture<JdListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JdListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JdListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
