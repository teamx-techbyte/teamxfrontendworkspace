import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import {ResumeService } from './scan-jd.service';

@Component({
  selector: 'app-scan-jd',
  templateUrl: './scan-jd.component.html',
  styleUrls: ['./scan-jd.component.css']
})
export class ScanJdComponent implements OnInit {
  
  submitted = false;
 

  constructor(private formBuilder: FormBuilder,private resumeService : ResumeService) { }

  ngOnInit() {

  }
  registerForm = this.formBuilder.group({
    profile: ['', Validators.required],
    description: ['', Validators.required],
    technicalskill: ['', Validators.required],
    softskill: ['', [Validators.required]],
    experience: ['', [Validators.required]],
    education: ['', Validators.required],
    jobtype: ['', Validators.required],
    location: ['', Validators.required]
});
  // convenience getter for easy access to form fields

  submitForm() {
      this.submitted = true;
  }

  
  get f() { return this.registerForm.controls; }

  fileChange(event: any) {
   
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
        const file: File = fileList[0];
        const formData:FormData = new FormData();
        formData.append('file', file, file.name);       
       
        this.resumeService.uplaodResume(formData).subscribe(res =>{
          console.log(res)
        });
}


}

}
