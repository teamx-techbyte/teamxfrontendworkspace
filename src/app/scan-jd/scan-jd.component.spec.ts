import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanJdComponent } from './scan-jd.component';

describe('ScanJdComponent', () => {
  let component: ScanJdComponent;
  let fixture: ComponentFixture<ScanJdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScanJdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanJdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
