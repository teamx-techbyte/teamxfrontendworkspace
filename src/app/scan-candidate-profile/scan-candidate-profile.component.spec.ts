import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanCandidateProfileComponent } from './scan-candidate-profile.component';

describe('ScanCandidateProfileComponent', () => {
  let component: ScanCandidateProfileComponent;
  let fixture: ComponentFixture<ScanCandidateProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScanCandidateProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanCandidateProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
