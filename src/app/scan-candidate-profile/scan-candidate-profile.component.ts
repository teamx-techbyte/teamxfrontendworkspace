import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-scan-candidate-profile',
  templateUrl: './scan-candidate-profile.component.html',
  styleUrls: ['./scan-candidate-profile.component.css']
})
export class ScanCandidateProfileComponent implements OnInit {

 
  submitted = false;
 

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {

  }
  registerForm = this.formBuilder.group({
    
    candidatename: ['', Validators.required],
    email: ['', Validators.required],
    profile: ['', Validators.required],
    description: ['', Validators.required],
    technicalskill: ['', Validators.required],
    softskill: ['', [Validators.required]],
    experience: ['', [Validators.required]],
    education: ['', Validators.required],
    jobtype: ['', Validators.required],
    candidatelocation: ['', Validators.required]
});
  // convenience getter for easy access to form fields

  submitForm() {
    if(this.registerForm.valid) {
      this.submitted = true;
    } 
     
   

  }

  
  get f() { return this.registerForm.controls; }

  fileChange(event: any) {
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        let headers = new Headers();
        /** In Angular 5, including the header Content-Type can invalidate your request */
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
       

}
}


}
