import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { authInterceptorProviders } from './shared/services/http.interceptor';
import { LoginComponent } from './app/login/login.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { ScanCandidateProfileComponent } from './app/scan-candidate-profile/scan-candidate-profile.component';
import { ScanJdComponent } from './app/scan-jd/scan-jd.component';
import { JdListingComponent } from './app/jd-listing/jd-listing.component';
import { CandidateDetailPageComponent } from './app/candidate-detail-page/candidate-detail-page.component';
@NgModule({
  declarations: [
    AppComponent, 
    DashboardComponent, LoginComponent, 
     ScanCandidateProfileComponent,
     ScanJdComponent,
     JdListingComponent,
     CandidateDetailPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,   
    FormsModule,   
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
        
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent],
  
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
